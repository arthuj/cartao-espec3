package br.com.cartoes.services;

import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.Cliente;
import br.com.cartoes.models.dtos.CartaoEntradaDTO;
import br.com.cartoes.models.dtos.CartaoGetDTO;
import br.com.cartoes.models.dtos.CartaoSaidaDTO;
import br.com.cartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;
    @Autowired
    ClienteService clienteService;

    public CartaoSaidaDTO criarCartao(CartaoEntradaDTO cartaoEntradaDTO){
        Optional<Cliente> optionalCliente = clienteService.buscarCliente(cartaoEntradaDTO.getClienteId());
        if(optionalCliente.isPresent())
        {
            Cartao cartao = new Cartao();
            cartao.setNumero(cartaoEntradaDTO.getNumero());
            cartao.setCliente(optionalCliente.get());
            cartao.setAtivo(false);
            cartao = cartaoRepository.save(cartao);
            CartaoSaidaDTO cartaoSaidaDTO = new CartaoSaidaDTO();
            return cartaoSaidaDTO.traduzirCartaoParaDTO(cartao);
        }
        else
            throw new RuntimeException("Cliente não existe");
    }

    public CartaoSaidaDTO ativarCartao(String numero, boolean ativar){
        Optional<Cartao> optionalCartao = cartaoRepository.findByNumero(numero);
        if(optionalCartao.isPresent()){
            Cartao cartao = optionalCartao.get();
            cartao.setAtivo(ativar);
            cartao = cartaoRepository.save(cartao);
            CartaoSaidaDTO cartaoSaidaDTO = new CartaoSaidaDTO();
            return cartaoSaidaDTO.traduzirCartaoParaDTO(cartao);
        }
        else
            throw new RuntimeException("Cartão não existe");
    }

    public Optional<Cartao>  buscarCartao(int id){
        Optional<Cartao> optionalCartao = cartaoRepository.findById(id);
        return optionalCartao;
    }
}
