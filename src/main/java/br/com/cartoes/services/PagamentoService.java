package br.com.cartoes.services;

import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.Pagamento;
import br.com.cartoes.models.dtos.CartaoGetDTO;
import br.com.cartoes.models.dtos.PagamentoEntradaCriarDTO;
import br.com.cartoes.models.dtos.PagamentoSaidaCriarDTO;
import br.com.cartoes.repositories.CartaoRepository;
import br.com.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;
    @Autowired
    CartaoService cartaoService;

    public Pagamento criarPagamento(PagamentoEntradaCriarDTO pagamentoEntradaCriarDTO){
        Optional<Cartao> optionalCartao = cartaoService.buscarCartao(pagamentoEntradaCriarDTO.getCartao_id());
        if(optionalCartao.isPresent()){
            Pagamento pagamento = new Pagamento();
            pagamento.setCartao(optionalCartao.get());
            pagamento.setDescricao(pagamentoEntradaCriarDTO.getDescricao());
            pagamento.setValor(pagamentoEntradaCriarDTO.getValor());
            return pagamentoRepository.save(pagamento);
        }
        else
            throw new RuntimeException("Cartão não existe");

    }

    public Iterable<Pagamento> buscarPorIdCartao(int id){
        Optional<Cartao> optionalCartao = cartaoService.buscarCartao(id);
        if(optionalCartao.isPresent()){
            return pagamentoRepository.findAllByCartao_id(id);
        }
        else
            throw new RuntimeException("Cartão não existe");
    }
}
