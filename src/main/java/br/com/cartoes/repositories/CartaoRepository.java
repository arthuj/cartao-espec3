package br.com.cartoes.repositories;

import br.com.cartoes.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao,Integer> {
    Optional<Cartao> findByNumero(String numero);
}
