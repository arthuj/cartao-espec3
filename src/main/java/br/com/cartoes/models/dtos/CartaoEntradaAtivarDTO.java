package br.com.cartoes.models.dtos;

public class CartaoSaidaAtivarDTO {
    private boolean ativo;

    public CartaoSaidaAtivarDTO() {
    }

    public CartaoSaidaAtivarDTO(boolean ativo) {
        this.ativo = ativo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
