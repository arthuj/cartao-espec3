package br.com.cartoes.models.dtos;

import br.com.cartoes.models.Pagamento;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class PagamentoSaidaCriarDTO {

    private int id;
    private int cartao_id;
    @NotBlank
    private String descricao;
    @NotNull
    private double valor;

    public PagamentoSaidaCriarDTO(int id, int cartao_id, @NotBlank String descricao, @NotNull double valor) {
        this.id = id;
        this.cartao_id = cartao_id;
        this.descricao = descricao;
        this.valor = valor;
    }

    public PagamentoSaidaCriarDTO() {
    }

    public PagamentoSaidaCriarDTO traduzirParaDTO(Pagamento pagamento){
        PagamentoSaidaCriarDTO pagamentoSaidaCriarDTO = new  PagamentoSaidaCriarDTO(pagamento.getId(),pagamento.getCartao().getId()
                ,pagamento.getDescricao(),pagamento.getValor());
        return pagamentoSaidaCriarDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
