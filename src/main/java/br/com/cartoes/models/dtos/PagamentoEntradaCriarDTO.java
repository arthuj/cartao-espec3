package br.com.cartoes.models.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PagamentoEntradaCriarDTO {

    private int cartao_id;
    @NotBlank
    private String descricao;
    @NotNull
    private double valor;

    public PagamentoEntradaCriarDTO(int cartao_id, @NotBlank String descricao, @NotNull double valor) {
        this.cartao_id = cartao_id;
        this.descricao = descricao;
        this.valor = valor;
    }

    public PagamentoEntradaCriarDTO() {
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
