package br.com.cartoes.models.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CartaoEntradaDTO {

    @NotBlank
    private String numero;
    @NotNull
    private int clienteId;

    public CartaoEntradaDTO(@NotBlank String numero, @NotNull int clienteId) {
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public CartaoEntradaDTO() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
