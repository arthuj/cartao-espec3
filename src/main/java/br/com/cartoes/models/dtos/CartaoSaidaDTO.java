package br.com.cartoes.models.dtos;

import br.com.cartoes.models.Cartao;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CartaoSaidaDTO {

    private int id;
    private String numero;
    private int clienteId;
    private boolean ativo;

    public CartaoSaidaDTO(int id, String numero, int clienteId, boolean ativo) {
        this.id = id;
        this.numero = numero;
        this.clienteId = clienteId;
        this.ativo = ativo;
    }

    public CartaoSaidaDTO() {
    }

    public CartaoSaidaDTO traduzirCartaoParaDTO(Cartao cartao){
        CartaoSaidaDTO cartaoSaidaDTO = new CartaoSaidaDTO(cartao.getId(),cartao.getNumero(),
                cartao.getCliente().getId(), cartao.isAtivo());
        return cartaoSaidaDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
