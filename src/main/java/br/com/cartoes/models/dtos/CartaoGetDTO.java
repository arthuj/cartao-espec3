package br.com.cartoes.models.dtos;

import br.com.cartoes.models.Cartao;

public class CartaoGetDTO {

    private int id;
    private String numero;
    private int clienteId;

    public CartaoGetDTO(int id, String numero, int clienteId) {
        this.id = id;
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public CartaoGetDTO() {
    }

    public CartaoGetDTO traduzirCartaoParaDTO(Cartao cartao){
        CartaoGetDTO cartaoGetDTO = new CartaoGetDTO(cartao.getId(),cartao.getNumero(),
                cartao.getCliente().getId());
        return cartaoGetDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
