package br.com.cartoes.controllers;

import br.com.cartoes.models.Cliente;
import br.com.cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Cliente criarCliente(@RequestBody @Valid Cliente cliente){
        Cliente clienteObjeto = clienteService.criarCliente(cliente);
        return clienteObjeto;
    }

    @GetMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public Cliente buscarCliente(@PathVariable(name = "id") int id){
        try {
            Optional<Cliente> optionalCliente = clienteService.buscarCliente(id);
            return optionalCliente.get();
        }catch (RuntimeException ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
