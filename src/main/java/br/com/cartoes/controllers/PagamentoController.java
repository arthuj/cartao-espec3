package br.com.cartoes.controllers;

import br.com.cartoes.models.Pagamento;
import br.com.cartoes.models.dtos.PagamentoEntradaCriarDTO;
import br.com.cartoes.models.dtos.PagamentoSaidaCriarDTO;
import br.com.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoSaidaCriarDTO criarPagamento(@RequestBody @Valid PagamentoEntradaCriarDTO pagamentoEntradaCriarDTO){
        Pagamento pagamento = pagamentoService.criarPagamento(pagamentoEntradaCriarDTO);
        PagamentoSaidaCriarDTO pagamentoSaidaCriarDTO = new PagamentoSaidaCriarDTO();
        return pagamentoSaidaCriarDTO.traduzirParaDTO(pagamento);
    }

    @GetMapping("/pagamentos/{id_cartao}")
    @ResponseStatus(HttpStatus.OK)
    public List<PagamentoSaidaCriarDTO> buscarPagamentos(@PathVariable(name = "id_cartao") int id){
        try {
            List<PagamentoSaidaCriarDTO> listaPagamentoDTO = new ArrayList<>();
            Iterable<Pagamento> pagamentos = pagamentoService.buscarPorIdCartao(id);
            for (Pagamento pagamento: pagamentos) {
                PagamentoSaidaCriarDTO pagamentoSaidaCriarDTO = new PagamentoSaidaCriarDTO();
                pagamentoSaidaCriarDTO = pagamentoSaidaCriarDTO.traduzirParaDTO(pagamento);
                listaPagamentoDTO.add(pagamentoSaidaCriarDTO);
            }
            return listaPagamentoDTO;
        }catch (RuntimeException ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }

    }

}
