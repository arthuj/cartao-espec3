package br.com.cartoes.controllers;

import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.dtos.CartaoEntradaDTO;
import br.com.cartoes.models.dtos.CartaoGetDTO;
import br.com.cartoes.models.dtos.CartaoSaidaDTO;
import br.com.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    CartaoService cartaoService;

    @PostMapping()
    @ResponseStatus(code = HttpStatus.CREATED)
    public CartaoSaidaDTO criarCartao(@RequestBody @Valid CartaoEntradaDTO cartaoEntradaDTO){
        return cartaoService.criarCartao(cartaoEntradaDTO);
    }

    @PatchMapping("/{numero}")
    @ResponseStatus(code = HttpStatus.OK)
    public CartaoSaidaDTO ativarCartao(@PathVariable(name = "numero") String numero, @RequestBody boolean ativo){
        try {
            CartaoSaidaDTO cartaoSaidaDTO = cartaoService.ativarCartao(numero,ativo);
            return cartaoSaidaDTO;
        }
        catch (RuntimeException ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CartaoGetDTO buscarCartao(@PathVariable(name = "id") int id){
        try{
            Optional<Cartao> cartao = cartaoService.buscarCartao(id);
            if(cartao.isPresent()){
                CartaoGetDTO cartaoGetDTO = new CartaoGetDTO();
                return cartaoGetDTO.traduzirCartaoParaDTO(cartao.get());
            }
            else
                throw new RuntimeException("Cartão não existe");
        }catch (RuntimeException ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }


    }

}
